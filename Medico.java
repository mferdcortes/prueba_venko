public class Medico {
    private String primerNombre;
    private String segundoNombre;
    private String primerApellido;
    private String segundoApellido;
    private String TipoDocumento;
    private String nDocumento;
    private String fechaExp;
    
    //clase medico sin atributos
    public Medico(){}
        
  //atributos de la clase médico
    public Medico(String primerNombre, String segundoNombre, String primerApellido, String segundoApellido, String TipoDocumento, String nDocumento, String fechaExp
    ){
        this.primerNombre = primerNombre;
        this.segundoNombre = segundoNombre;
        this.primerApellido = primerApellido;
        this.segundoApellido = segundoApellido;
        this.TipoDocumento = TipoDocumento;
        this.nDocumento = nDocumento;
        this.fechaExp = fechaExp;
    }

    //asignación de atributos a la clase con metodos set y get
    
    public String getPrimerNombre(){
        return this.primerNombre;
    }
    
    public void setPrimerNombre(String primerNombre) {
        this.primerNombre = primerNombre;
    }
    
    public String getsegundoNombre(){
        return this.segundoNombre;
    }
    
    public void setsegundoNombre(String segundoNombre) {
        this.segundoNombre = segundoNombre;
    }

    public String getprimerApellido(){
        return this.primerApellido;
    }
    
    public void setprimerApellido(String primerApellido) {
        this.primerApellido = primerApellido;
    }

    public String getsegundoApellido(){
        return this.segundoApellido;
    }
    
    public void setsegundoApellido(String segundoApellido) {
        this.segundoApellido = segundoApellido;
    }

    public String getTipoDocumento(){
        return this.TipoDocumento;
    }
    
    public void setTipoDocumento(String TipoDocumento) {
        this.TipoDocumento = TipoDocumento;
    }

    public String getnDocumento(){
        return this.nDocumento;
    }
    
    public void setnDocumento(String nDocumento) {
        this.nDocumento = nDocumento;
    }

    public String getfechaExp(){
        return this.fechaExp;
    }
    
    public void setfechaExp(String fechaExp) {
        this.fechaExp = fechaExp;
    }
}
