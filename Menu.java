import java.util.ArrayList;
import java.util.Scanner;

public class Menu {
    public static void main(String[] args) {
        Scanner entrada = new Scanner(System.in);
        // El while será ejecutado hasta que salir sea TRUE.
        boolean salir = false;
        while (!salir) {   
            // MENU PRINCIPAL
            System.out.println(" PRUEBA TÉCNICA  - VENKO ");
            System.out.println(" 1 - MENÚ MÉDICOS ");
            System.out.println(" 2 - MENÚ PACIENTES");
            System.out.println(" 0 - Salir ");
            
        
            int opcion = Integer.parseInt(entrada.nextLine());
           
            switch(opcion) {
                case 1: 
                    MenuMedico();
                    break;
                case 2: 
                    MenuPaciente();
                    break;
               
                case 0://Salir del programa
                    salir = true;
                    System.out.println("SALIR DEL PROGRAMA");
                    break;
                default: 
                    System.out.println("Opción inválida");
                
                    
            }
            
        }
        
        
    }
    public static void MenuMedico() {
        Scanner entrada = new Scanner(System.in);
        // LISTA DE MEDICOS
        ArrayList<Medico> listaMedicos = new ArrayList<>();
        boolean salir = false;
        while (!salir) {   
            // SUBMENU MEDICO
            System.out.println(" MENÚ MÉDICO ");
            System.out.println(" 1 - Nuevo registro + ");
            System.out.println(" 2 - Lista de registros");
            System.out.println(" 3 - Eliminar registro ");
            System.out.println(" 4 - Modificar registro ");
            System.out.println(" 0 - Salir ");
                   
            int opcion = Integer.parseInt(entrada.nextLine());
            switch(opcion) {
                case 1:
                    Medico med = new Medico();
                    System.out.println("Ingrese número de documento del usuario:");
                    med.setnDocumento(entrada.nextLine());
                    
                    System.out.println("Ingrese primero nombre: ");
                    med.setPrimerNombre(entrada.nextLine());
                    
                    System.out.println("Ingrese segundo nombre: ");
                    med.setsegundoNombre(entrada.nextLine());
                   
                    System.out.println("Ingrese primer apellido: ");
                    med.setprimerApellido(entrada.nextLine());
                   
                    System.out.println("Ingrese segundo apellido: ");
                    med.setsegundoApellido(entrada.nextLine());
                    
                    System.out.println("Ingrese tipo de documento: ");
                    System.out.println("Cedula");
                    System.out.println("Tarjetai");
                    System.out.println("Pasaporte");
                    med.setTipoDocumento(entrada.nextLine());
                    
                    System.out.println("Ingrese Fecha de expedición del documento: ");
                    med.setfechaExp(entrada.nextLine());
                    

                    //Guardar registro medico a la lista con .add
                    listaMedicos.add(med);
                    
                    // Retornar el registro creado de medico
                    
                    System.out.println("REGISTRO CREADO MÉDICO: ");
                    System.out.println("Primer nombre " + med.getPrimerNombre());
                    System.out.println("Segundo nombre " + med.getsegundoNombre());
                    System.out.println("Primer apellido " + med.getprimerApellido());
                    System.out.println("Segundo apellido " + med.getsegundoApellido());
                    System.out.println("Tipo de documento " + med.getTipoDocumento());
                    System.out.println("Fecha expedición documento " + med.getfechaExp());
                    
                    
                    break;
                case 2: 
                //recorrer lista medicos
                   for(int i = 0; i < listaMedicos.size(); i++){ 
                       //.size permite ver la cantidad de registros almacenados en lista
                       System.out.println("________________________");
                       Medico medic =  listaMedicos.get(i); 
                
                    System.out.println("Número de documento :" + medic.getnDocumento());
                    System.out.println("Primer nombre :" + medic.getPrimerNombre());
                    System.out.println("Segundo nombre :" + medic.getsegundoNombre());
                    System.out.println("Primer apellido :" + medic.getprimerApellido());
                    System.out.println("Segundo apellido :" + medic.getsegundoApellido());
                    System.out.println("Tipo de documento :" + medic.getTipoDocumento());
                    System.out.println("Fecha expedición documento " + medic.getfechaExp());
                       
                   }
                    
                    break;
        
                case 3:
                    System.out.println("Ingrese el documento del registro a eliminar: ");
                    String nDocumento = entrada.nextLine();
                    
                    for (int i = 0; i < listaMedicos.size(); i++) {
                        Medico medic = listaMedicos.get(i);
                        //Buscar documento que coincida en la lista
                        if (nDocumento.equals(medic.getnDocumento())) {
                            // Eliminar registro
                            listaMedicos.remove(i);
                            System.out.println("Registro eliminado");
                        }
                    }
                    
                    break;
                case 4:
                 // Modificar registro
                    System.out.println(" Ingrese el documento del registro a modificar");
                    nDocumento = entrada.nextLine();
                   
                    for (int i = 0; i < listaMedicos.size(); i++) {
                        Medico medic = listaMedicos.get(i);
                        if (nDocumento.equals(medic.getnDocumento())) {
                            System.out.println("Primer nombre :" + medic.getPrimerNombre());
                            System.out.println("Segundo nombre :" + medic.getsegundoNombre());
                            System.out.println("Primer apellido :" + medic.getprimerApellido());
                            System.out.println("Segundo apellido :" + medic.getsegundoApellido());
                            System.out.println("Tipo de documento :" + medic.getTipoDocumento());
                            System.out.println("Fecha expedición documento :" + medic.getfechaExp());

                            System.out.println("Ingrese nuevamente primer nombre: ");
                            String primerNombre = entrada.nextLine();

                            System.out.println("Ingrese nuevamente segundo nombre: ");
                            String segundoNombre = entrada.nextLine();

                            System.out.println("Ingrese nuevamente primer apellido: ");
                            String primerApellido = entrada.nextLine();

                            System.out.println("Ingrese nuevamente segundo apellido: ");
                            String segundoApellido = entrada.nextLine();

                            System.out.println("Ingrese nuevamente tipo de documento: ");
                            System.out.println("Cédula de ciudadanía");
                            System.out.println("Tarjeta de identidad");
                            System.out.println("Pasaporte");
                            String TipoDocumento = entrada.nextLine();

                            System.out.println("Ingrese nuevamente fecha de expedición del documento: ");
                            String fechaExp = entrada.nextLine();
                           
                            
                            medic.setPrimerNombre(primerNombre);
                            medic.setsegundoNombre(segundoNombre);
                            medic.setprimerApellido(primerApellido);
                            medic.setsegundoApellido(segundoApellido);
                            medic.setTipoDocumento(TipoDocumento);
                            medic.setfechaExp(fechaExp);
                            
                        }
                    }
                    break;
                case 0:
                //Salir del programa
                    salir = true;
                    System.out.println("Salir del programa");
                    break;
                default:
                    System.out.println("Opcion no válida");
                
            }
        }
        
    }
    public static void MenuPaciente() {
        Scanner entrada = new Scanner(System.in);
        // LISTA DE MEDICOS
        ArrayList<Paciente> listaPacientes = new ArrayList<>();
        boolean salir = false;
        while (!salir) {   
            // SUBMENU MEDICO
            System.out.println(" MENÚ PACIENTE ");
            System.out.println(" 1 - Nuevo registro + ");
            System.out.println(" 2 - Lista de registros");
            System.out.println(" 3 - Eliminar registro ");
            System.out.println(" 4 - Modificar registro ");
            System.out.println(" 0 - Salir ");
                   
            int opcion = Integer.parseInt(entrada.nextLine());
            switch(opcion) {
                case 1:
                    Paciente pac = new Paciente();
                    System.out.println("Ingrese número de documento del usuario:");
                    pac.setnDocumento(entrada.nextLine());
                    
                    System.out.println("Ingrese primero nombre: ");
                    pac.setPrimerNombre(entrada.nextLine());
                    
                    System.out.println("Ingrese segundo nombre: ");
                    pac.setsegundoNombre(entrada.nextLine());
                   
                    System.out.println("Ingrese primer apellido: ");
                    pac.setprimerApellido(entrada.nextLine());
                   
                    System.out.println("Ingrese segundo apellido: ");
                    pac.setsegundoApellido(entrada.nextLine());
                    
                    System.out.println("Ingrese tipo de documento: ");
                    System.out.println("Cedula");
                    System.out.println("Tarjetai");
                    System.out.println("Pasaporte");
                    pac.setTipoDocumento(entrada.nextLine());
                    
                    System.out.println("Ingrese Fecha de expedición del documento: ");
                    pac.setfechaExp(entrada.nextLine());
                    

                    //Guardar registro paciente a la lista con .add
                    listaPacientes.add(pac);
                    
                    // Retornar el registro creado de paciente
                    
                    System.out.println("REGISTRO CREADO MÉDICO: ");
                    System.out.println("Primer nombre :" + pac.getPrimerNombre());
                    System.out.println("Segundo nombre :" + pac.getsegundoNombre());
                    System.out.println("Primer apellido :" + pac.getprimerApellido());
                    System.out.println("Segundo apellido :" + pac.getsegundoApellido());
                    System.out.println("Tipo de documento :" + pac.getTipoDocumento());
                    System.out.println("Fecha expedición documento :" + pac.getfechaExp());
                    
                    
                    break;
                case 2: 
                //recorrer lista medicos
                   for(int i = 0; i < listaPacientes.size(); i++){ 
                       //.size permite ver la cantidad de registros almacenados en lista
                       System.out.println("___________________");
                       Paciente paci =  listaPacientes.get(i); 
                       
                    System.out.println("Número de documento :" + paci.getnDocumento());
                    System.out.println("Primer nombre :" + paci.getPrimerNombre());
                    System.out.println("Segundo nombre :" + paci.getsegundoNombre());
                    System.out.println("Primer apellido :" + paci.getprimerApellido());
                    System.out.println("Segundo apellido :" + paci.getsegundoApellido());
                    System.out.println("Tipo de documento :" + paci.getTipoDocumento());
                    System.out.println("Fecha expedición documento :" + paci.getfechaExp());
                       
                   }
                    
                    break;
        
                case 3:
                    System.out.println("Ingrese el documento del registro a eliminar: ");
                    String nDocumento = entrada.nextLine();
                    
                    for (int i = 0; i < listaPacientes.size(); i++) {
                        Paciente paci = listaPacientes.get(i);
                        //Buscar documento que coincida en la lista
                        if (nDocumento.equals(paci.getnDocumento())) {
                            // Eliminar registro
                            listaPacientes.remove(i);
                            System.out.println("Registro eliminado");
                        }
                    }
                    
                    break;
                case 4:
                 // Modificar registro
                    System.out.println(" Ingrese el documento del registro a modificar");
                    nDocumento = entrada.nextLine();
                   
                    for (int i = 0; i < listaPacientes.size(); i++) {
                        Paciente paci = listaPacientes.get(i);
                        if (nDocumento.equals(paci.getnDocumento())) {
                            System.out.println("Primer nombre :" + paci.getPrimerNombre());
                            System.out.println("Segundo nombre :" + paci.getsegundoNombre());
                            System.out.println("Primer apellido :" + paci.getprimerApellido());
                            System.out.println("Segundo apellido :" + paci.getsegundoApellido());
                            System.out.println("Tipo de documento :" + paci.getTipoDocumento());
                            System.out.println("Fecha expedición documento :" + paci.getfechaExp());

                            System.out.println("Ingrese nuevamente primer nombre: ");
                            String primerNombre = entrada.nextLine();

                            System.out.println("Ingrese nuevamente segundo nombre: ");
                            String segundoNombre = entrada.nextLine();

                            System.out.println("Ingrese nuevamente primer apellido: ");
                            String primerApellido = entrada.nextLine();

                            System.out.println("Ingrese nuevamente segundo apellido: ");
                            String segundoApellido = entrada.nextLine();

                            System.out.println("Ingrese nuevamente tipo de documento: ");
                            System.out.println("Cédula de ciudadanía");
                            System.out.println("Tarjeta de identidad");
                            System.out.println("Pasaporte");
                            String TipoDocumento = entrada.nextLine();

                            System.out.println("Ingrese nuevamente fecha de expedición del documento: ");
                            String fechaExp = entrada.nextLine();
                           
                            
                            paci.setPrimerNombre(primerNombre);
                            paci.setsegundoNombre(segundoNombre);
                            paci.setprimerApellido(primerApellido);
                            paci.setsegundoApellido(segundoApellido);
                            paci.setTipoDocumento(TipoDocumento);
                            paci.setfechaExp(fechaExp);
                            
                        }
                    }
                    break;
                case 0:
                //Salir del programa
                    salir = true;
                    System.out.println("Salir del programa");
                    break;
                default:
                    System.out.println("Opcion no válida");
                
            }
        }
        
    }
}